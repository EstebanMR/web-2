using GraphQLBookstore.Models;
using GraphQLBookstore.Repositories;
using GraphQL.Types;


namespace GraphQLBookstore.GraphQL.Types{
    class ProductType : ObjectGraphType<Product>
    {
        public ProductType(ProductRepository productRepository){
            Name = "Product";
            Field(x => x.Id);
            Field(x => x.Name).Description("Name of the product");
            Field(x => x.Description).Description("Description of the product");
            Field(x => x.Price).Description("Price of the product");
        }
    }
}