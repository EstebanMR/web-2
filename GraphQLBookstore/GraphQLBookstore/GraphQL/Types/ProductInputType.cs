using GraphQL.Types;
using GraphQLBookstore.Models;

namespace  GraphQLBookstore.GraphQL.Types 
{
    class ProductInputType : InputObjectGraphType
    {
        public ProductInputType()
        {
            Name = "ProductInput";
            Field<NonNullGraphType<StringGraphType>>("name");
            Field<NonNullGraphType<StringGraphType>>("description");
            Field<NonNullGraphType<IntGraphType>>("price");
        }
    }
}