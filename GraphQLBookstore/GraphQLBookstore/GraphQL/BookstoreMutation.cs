using GraphQL.Types;
using GraphQLBookstore.Repositories;
using GraphQLBookstore.GraphQL.Types;
using GraphQLBookstore.Models;

namespace  GraphQLBookstore.GraphQL {
    class BookstoreMutation : ObjectGraphType
    {
        public BookstoreMutation(ProductRepository productRepository)
        {
            //create products
            Field<ProductType>("createProduct",
                              arguments: new QueryArguments(new QueryArgument<NonNullGraphType<ProductInputType>> { Name = "input" }),
                              resolve: context => {
                                  return productRepository.Add(context.GetArgument<Product>("input"));
                              });
            //delete departaments
            Field<ProductType>("deletePrduct",
                              arguments: new QueryArguments(new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id" }),
                              resolve: context => {
                                  return productRepository.Remove(context.GetArgument<long>("id"));
                              }); 
        }
    }
}