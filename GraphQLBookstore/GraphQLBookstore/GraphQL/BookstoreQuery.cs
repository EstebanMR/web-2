using GraphQL.Types;
using GraphQLBookstore.Repositories;
using GraphQLBookstore.GraphQL.Types;

namespace  GraphQLBookstore.GraphQL {
    class BookstoreQuery : ObjectGraphType
    {
        public BookstoreQuery(ProductRepository productRepository)
        {
            Field<ListGraphType<ProductType>>("products",
                                             arguments: new QueryArguments(
                                                new QueryArgument<StringGraphType> { Name = "desciption" },
                                                new QueryArgument<BooleanGraphType> { Name = "order" }),
                                             resolve: context => {
                                                 return productRepository.All(context);
                                             });
        }
    }
}