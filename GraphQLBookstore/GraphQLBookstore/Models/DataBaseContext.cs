using Microsoft.EntityFrameworkCore;

namespace GraphQLBookstore.Models
{
    public class DataBaseContext : DbContext
    {
        public DataBaseContext(DbContextOptions<DataBaseContext> options) : base(options)
        {
            
        }
        public DbSet<Product> Products { get; set; }
    }
}