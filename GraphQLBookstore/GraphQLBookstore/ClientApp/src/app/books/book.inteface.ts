export interface Book {
    id: number;
    name: string;
    description: string;
    price: number;
    author: string;
    authorid: number;
}