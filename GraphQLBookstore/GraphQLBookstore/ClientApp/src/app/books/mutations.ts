import gql from 'graphql-tag';

export const CREATE_BOOK = gql`
    mutation($name:String!, $description:String!, $price:Float!, $authorId:Int!){
        createBook(input:{name:$name, description:$description, price:$price, authorId:$authorId}){
            id 
            name
        }
    }
`;

export const UPDATE_BOOK = gql`
    mutation($id: ID!, $name:String!, $description:String!, $price:Float!, $authorId:Int!){
        updateBook(id: $id, input:{name:$name, description:$description, price:$price, authorId:$authorId}){
            id
            name
        }
    }
`;

export const DELETE_BOOK = gql`
    mutation($id:ID!){
        deleteBook(id: $id){
            id
            name
        }
    }
`;