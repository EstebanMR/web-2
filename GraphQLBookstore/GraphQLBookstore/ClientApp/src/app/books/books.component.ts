import { Component } from '@angular/core';
import { BOOKS_QUERY } from './queries';
import { CREATE_BOOK, UPDATE_BOOK, DELETE_BOOK } from './mutations';
import { AUTHORS_QUERY } from '../author/queries';
import { Book } from './book.inteface';
import { Author } from '../author/author.inteface';
import { Apollo } from 'apollo-angular';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent {

  books: Book[];
  authors: Author[];
  is_editting = false;
  currentBook: Book;
  currentAuthor: Author;
  constructor(private apollo: Apollo) {
    this.currentBook = { id: -1, name: '', description: '', price: 0, author :"", authorid:-1};
    this.currentAuthor = { id: -1, name: '' };
    this.filter();
  }

  edit(book: Book){
    this.currentBook = {...book};
    this.is_editting = true;
  }

  delete(book: Book){
    this.apollo.mutate({
      mutation: DELETE_BOOK,
      variables: { id: book.id }
    }).subscribe(() => {
      this.currentBook = { id: -1, name: '', description: '', price: 0, author :"",  authorid:-1};
      this.currentAuthor = { id: -1, name: '' };
      this.is_editting = false;
      this.filter();
    });
  }

  save() {
    this.apollo.watchQuery({
      query: AUTHORS_QUERY,
      fetchPolicy: 'network-only',
      variables: {
        name: this.currentAuthor.name
      }
    }).valueChanges.subscribe(result => {
      var temp = result.data['authors'];
      this.currentAuthor.id = temp[0].id;
      this.currentBook.authorid = temp[0].id;
      console.log( this.currentBook);
      console.log(this.currentAuthor);
      let mutation = CREATE_BOOK;
      const variables = {
        input: { name: this.currentBook.name+"", description: this.currentBook.description, price: this.currentBook.price, authorid:this.currentAuthor.id }
      };
      if(this.currentBook.id > 0){
        variables['id'] = this.currentBook.id;
        mutation = UPDATE_BOOK;
      }
      console.log(variables);
      console.log(mutation);
      this.apollo.mutate({
        mutation: mutation,
        variables: variables
      }).subscribe(() => {
        this.currentBook = { id: -1, name: '', description: '', price: 0, author :"", authorid:-1};
        this.currentAuthor = { id: -1, name: '' };
        this.is_editting = false;
        this.filter();
      });
    });
    
  }

  filter() {
    this.apollo.watchQuery({
      query: BOOKS_QUERY,
      fetchPolicy: 'network-only',
      variables: {}
    }).valueChanges.subscribe(result => {
      this.books = result.data['books'];
    });

    this.apollo.watchQuery({
      query: AUTHORS_QUERY,
      fetchPolicy: 'network-only',
      variables: {}
    }).valueChanges.subscribe(result => {
      this.authors = result.data['authors'];
    });
  }
}
