import gql from 'graphql-tag';

export const CREATE_PRODUCT = gql`
    mutation($name: String!, $des:String!, $price:Int!){
        createProduct(input: {name:$name, description:$des, price: $price}){
            id
            name
            description
            price
        }
    }
`;

export const DELETE_PRODUCT = gql`
    mutation($id:ID!){
        deletePrduct(id:$id){
            id
            name
            description
            price
        }
    }
`;