import gql from 'graphql-tag';

export const PRODUCTS_QUERY = gql`
    query($des:String, $or: Boolean){
        products(desciption:$des, order:$or){
            id
            name
            description
            price
        }
    }
`;