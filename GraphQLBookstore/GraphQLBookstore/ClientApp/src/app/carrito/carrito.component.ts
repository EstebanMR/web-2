import { Component } from '@angular/core';
import { PRODUCTS_QUERY } from './Queries';
import { CREATE_PRODUCT, DELETE_PRODUCT } from './Mutations';
import { Product } from './carrito.interface';
import { Apollo } from 'apollo-angular';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.css']
})
export class CarritoComponent{

  products: Product[];
  description_filter = '';
  is_editting = false;
  currentProduct: Product;
  confirmation=false;

  constructor(private apollo: Apollo) { 
    this.currentProduct = { id: -1, name: '', description: '', price: 0};
    this.filter();
  }

  delete(product: Product){
    if (window.confirm("¿Desea eliminar el producto: "+product.name+"?")) {
      this.apollo.mutate({
        mutation: DELETE_PRODUCT,
        variables: { id: product.id }
      }).subscribe(() => {
        this.currentProduct = { id: -1, name: '', description: '', price: 0};
        this.is_editting = false;
        this.filter();
      });
    }
  }

  save() {
    console.log(this.currentProduct);
    this.apollo.mutate({
      mutation: CREATE_PRODUCT,
      variables: {name: this.currentProduct.name, des: this.currentProduct.description, price: this.currentProduct.price 
      }
    }).subscribe(() => {
      this.currentProduct = { id: -1, name: '', description: '', price: 0};
      this.is_editting = false;
      this.filter();
    });
  }

  filter() {
    this.apollo.watchQuery({
      query: PRODUCTS_QUERY,
      fetchPolicy: 'network-only',
      variables: {
        des: this.description_filter
      }
    }).valueChanges.subscribe(result => {
      this.products = result.data['products'];
    }); 
  }

  sort(){
    if (this.confirmation) {
      this.apollo.watchQuery({
        query: PRODUCTS_QUERY,
        fetchPolicy: 'network-only',
        variables: {
          des: this.description_filter,
          or: false
        }
      }).valueChanges.subscribe(result => {
        this.products = result.data['products'];
        console.log(this.products);
      }); 
      this.confirmation = false;
    } else if (!this.confirmation) {
      this.apollo.watchQuery({
        query: PRODUCTS_QUERY,
        fetchPolicy: 'network-only',
        variables: {
          des: this.description_filter,
          or: true
        }
      }).valueChanges.subscribe(result => {
        this.products = result.data['products'];
        console.log(this.products);
      }); 
      this.confirmation = true;
    }
  }
}
