using GraphQLBookstore.Models;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using GraphQL.Types;

namespace GraphQLBookstore.Repositories
{
    class ProductRepository
    {
        private readonly DataBaseContext _context;
        public ProductRepository(DataBaseContext context)
        {
            _context = context;
        }

       public IEnumerable<Product> All(ResolveFieldContext<object> context){
            var results = from products in _context.Products select products;
            if (context.HasArgument("desciption"))
            {
                var value = context.GetArgument<string>("desciption");
                results = results.Where(a => a.Description.Contains(value));
            }
            if (context.HasArgument("order"))
            {
                if (context.GetArgument<bool>("order"))
                {
                    results = results.OrderBy(a => a.Name);
                }
                if (!context.GetArgument<bool>("order"))
                {
                    results = results.OrderByDescending(a => a.Name);
                }
            }
            return results;
        }

        public async Task<Product> Add(Product product) {
            _context.Products.Add(product);
            await _context.SaveChangesAsync();
            return product;
        }

        public async Task<Product> Remove(long id) {
            var product = await _context.Products.FindAsync(id);
            if (product == null)
            {
                return null;
            }
            _context.Products.Remove(product);
            await _context.SaveChangesAsync();
            return product;
        } 

    }
}
